'use strict'
import React,{Component} from 'react'
import {
	StyleSheet,
	Dimensions,
	Platform,
	View,
	Text,
	Image,
	TouchableHighlight
} from 'react-native'
import Env from '../../utils/env'
import { HEIGHT_TOPBAR, MAIN, DETAIL, AVATAR } from '../../utils/util'

var {height, width} = Dimensions.get('window')

export default class Topbar extends Component{
	constructor(props){
		super(props)
		this.state = {
			selectedTab:0,
			routeNameFeed:MAIN,
			routeNameNotif:MAIN,
			routeNameMessage:MAIN,
			routeNameProfile:MAIN,
			stackProfile:null
		}
	}

	setSelectedTab(tab){
		this.setState({selectedTab:tab})
	}

	componentDidMount(){

	}

	render(){
		var tab = this.state.selectedTab
		var title = "Question"
		if(tab === 0){
			title = "Question"
		} else if(tab === 1){
			title = "Notification"
			//return null
		} else if(tab === 2){
			title = "Message"
			//return null
		} else if(tab === 3){
			title = "Profile"
			//return null
		}
		return(
			<View style={{width:width,height:HEIGHT_TOPBAR,backgroundColor:'#ffffff',paddingTop:Platform.OS === 'ios' ? 32 : 0,alignItems:'center',justifyContent:'center'}}>
				<Text style={{color:'#000000',fontSize:30,fontWeight:'bold'}}>{title}</Text>
			</View>
		)
	}
}
