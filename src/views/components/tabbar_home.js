'use strict'
import React,{Component} from 'react'
import {
	StyleSheet,
	Dimensions,
	Animated,
	Easing,
	View,
	Text,
	Image,
	TouchableHighlight
} from 'react-native'
import Env from '../../utils/env'
import { LOGO_COLOR } from '../../utils/util'

var {height, width} = Dimensions.get('window')

export default class HomeTabbar extends Component{
	constructor(props){
		super(props)
		this.state = {
			show:false,
			visible:true,
			selected:0
		}
		this.RotateValueHolder = new Animated.Value(1);
		this.left = new Animated.Value(width/2 -30);
		this.right = new Animated.Value(width/2 -30);
	}

	componentDidMount(){
		Env.getInstance().setTabbar(this)
	}

	show(){
		if(!this.state.visible)
			this.setState({visible:true})
	}

	hide(){
		if(this.state.visible)
			this.setState({visible:false})
	}

	startImageRotateFunction () {
	  this.RotateValueHolder.setValue(0)
	  var left = this.state.show ? width/2 -30 : 10
		var	right = this.state.show ? width/2 -30 : 10

	  Animated.parallel([
		  Animated.spring(this.RotateValueHolder,{
	      toValue: 1,
	      duration: 300,
	      friction: 3,
	      init:true
	    }),
		  Animated.timing(this.left,{
	      toValue: left,
	      duration: 150,
	      easing: Easing.linear
	    }),
	    Animated.timing(this.right,{
	      toValue: right,
	      duration: 150,
	      easing: Easing.linear
	    })
		]).start();
	}

	componentDidUpdate(){
		if(this.state.init){
			this.setState({init:false})
		}
	}

	render(){
		if(!this.state.visible){
			return null
		}
		var outputRange = this.state.show === true ? ['0deg', '180deg'] : ['180deg', '360deg']

		const RotateData = this.RotateValueHolder.interpolate({
      inputRange: [0, 1],
      outputRange: outputRange
    })
    const left_right = { left: this.left, right:this.right }
		return(
			<Animated.View style={[styles.contain,left_right]}>
				{this.renderButton(0)}
				{this.renderButton(1)}
				<TouchableHighlight
					underlayColor='transparent'
					style={{flex:1,alignItems:'center',justifyContent:'center'}}
					onPress={()=>{
						this.startImageRotateFunction();
						this.setState({show:!this.state.show})
					}}>
					<Animated.Image
		        style={{
		          width: 40,
		          height: 40,
		          transform: [{rotate:RotateData},{perspective: 1000}]
		        }}
		        source={require('../../images/ico-white-logo-q.png')}
		      />
				</TouchableHighlight>
				{this.renderButton(2)}
				{this.renderButton(3)}
			</Animated.View>
		)
	}

	renderButton(index,onPress){
		var image = require('../../images/ico-dashboard.png')
		if(index === 1){
			image = require('../../images/ico-bell.png')
		} else if(index === 2){
			image = require('../../images/ico-message.png')
		} else if(index === 3){
			image = require('../../images/ico-profile.png')
		}
		if(this.state.show){
			return(
				<TouchableHighlight
					underlayColor='transparent'
					style={{flex:1}}
					onPress={()=>{
						this.setState({selected:index,show:!this.state.show})
						this.startImageRotateFunction();
						//console.log(this.props);
						this.props.jumpToIndex(index)
					}}>
					<View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
						<Image source={image} resizeMode='contain' style={{width:25,height:25}} />
						<View style={[styles.selected,{backgroundColor:this.state.selected === index ? '#ffffff' : LOGO_COLOR}]} />
					</View>
				</TouchableHighlight>
			)
		} else {
			return null
		}

	}
}

const styles = StyleSheet.create({
	contain:{
		position:'absolute',
		flexDirection:'row',
		alignItems:'stretch',
		//justifyContent:'space-between',
		height:60,
		bottom:20,
		paddingLeft:10,
		paddingRight:10,
		backgroundColor:LOGO_COLOR,
		borderRadius:30,
		justifyContent:'center'
	},
	selected:{
		width:4,
		height:4,
		borderRadius:2,
		position:'absolute',
		top:45
	}
})
