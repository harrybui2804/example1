'use trict'
import React,{Component} from 'react'
import{
	StyleSheet,
	View,
	Text
} from 'react-native'
import APIConfig from '../../utils/APIConfig'
import * as Fetch from '../../utils/fetch'
import Env from '../../utils/env'
import * as Util from '../../utils/util'
import * as Toast from '../../utils/ToastUtil'
import { TabNavigator, TabBarBottom, NavigationActions } from 'react-navigation'
import Topbar from './topbar'
import HomeTabbar from './tabbar_home'

const Tab1View = ({ navigation }) => (
  <View style={{flex:1,alignItems:'center',justifyContent:'center'}} navigation={navigation} >
		<Text style={{fontSize:18,fontWeight:'bold'}}>Tab1</Text>
	</View>
);

const Tab2View = ({navigation}) =>(
	<View style={{flex:1,alignItems:'center',justifyContent:'center'}} navigation={navigation} >
		<Text style={{fontSize:18,fontWeight:'bold'}}>Tab2</Text>
	</View>
)

const Tab3View = ({navigation}) =>(
	<View style={{flex:1,alignItems:'center',justifyContent:'center'}} navigation={navigation} >
		<Text style={{fontSize:18,fontWeight:'bold'}}>Tab3</Text>
	</View>
)

const Tab4View = ({navigation}) =>(
	<View style={{flex:1,alignItems:'center',justifyContent:'center'}} navigation={navigation} >
		<Text style={{fontSize:18,fontWeight:'bold'}}>Tab4</Text>
	</View>
)

export default class Home extends Component{
	constructor(props){
		super(props)
		this.state = {
			currentTab:'',
      requestData:null
		}
    this._currentTab = 0
	}

  componentDidMount(){
    Env.getInstance().setSwitchNavigator(this.props.navigation)
  }

	render(){
		const HomeTab = TabNavigator(
      {
        Question     : { screen: Tab1View },
        Notification : { screen: Tab2View },
        Chat         : { screen: Tab3View },
        Profile      : { screen: Tab4View }
      },
      {/*
        navigationOptions: ({ navigation }) => ({
          tabBarIcon: ({ focused, tintColor }) => {
            return null
          }
        }),*/
        tabBarOptions: {
          activeTintColor: Util.LOGO_COLOR,
          inactiveTintColor: 'gray',
          showLabel:false,
          showIcon:false,
          style:{
            position:'absolute',
            bottom:-100,
            left:0,
            right:0
          }
        },
        initialRouteName:'Question',
        tabBarComponent: HomeTabbar,
        tabBarPosition: 'bottom',
        animationEnabled: false,
        swipeEnabled: false,
        lazy:true
      }
    )

		return(
			<View style={{flex:1,backgroundColor:'#ffffff'}}>
        <Topbar ref={comp => {
          this.topbar = comp
          Env.getInstance().setTopbar(comp)
        }} />
        <HomeTab onNavigationStateChange={this._onNavigationStateChange.bind(this)} />
			</View>
		)
	}

  _onNavigationStateChange(prevState, newState){
    var routeName = newState.routes[newState.index].routeName
    this.topbar.setSelectedTab(newState.index)
    this._currentTab = newState.index
  }
}
