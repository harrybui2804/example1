'use strict'

import React,{Component} from 'react'
import{
	ActivityIndicator,
	StyleSheet,
	View,
	Image,
	Text,
	TextInput,
	Keyboard,
	TouchableHighlight,
	TouchableWithoutFeedback
} from 'react-native'
import Env from '../../utils/env'
import APIConfig from '../../utils/APIConfig'
import * as Toast from '../../utils/ToastUtil'
import * as Util from '../../utils/util'
import * as Fetch from '../../utils/fetch'

export default class Login extends Component{
	constructor(props){
		super(props)
		this.state={
			email:'',
			pass:'',
			loading:false
		}
	}

	render(){
		return(
			<TouchableWithoutFeedback
				onPress={()=>{
					Keyboard.dismiss()
				}}>
				<View style={styles.contain}>
					<Image source={require('../../images/img-human-top.png')} resizeMode='contain' style={styles.human_top}/>
					<Image source={require('../../images/img-human-bottom.png')} resizeMode='contain' style={styles.human_bottom}/>
					<View style={{alignItems:'center',justifyContent:'center',marginTop:100}}>
						<Image source={require('../../images/ico-logo.png')} resizeMode='contain' style={{width:Util.DEVICE_SCREEN.width-80}}/>
						<Text style={{textAlign:'center'}}>Connecting live with an expert{'\n'}anytime anywhere</Text>
					</View>
					<View style={{flex:1,marginTop:50,paddingLeft:20,paddingRight:20}}>
		   			<View style={styles.viewInput}>
		   				<TextInput
								ref="emailInput"
								style={styles.textInput}
								underlineColorAndroid='transparent'
								autoCorrect={false}
								autoCapitalize='none'
								keyboardType='email-address'
								returnKeyType='next'
								clearButtonMode='while-editing'
								placeholder='Your email'
								placeholderTextColor='#cccccc'
				        onChangeText={(text) => this.setState({email:text.trim()})}
				        defaultValue={this.state.email}
				        onSubmitEditing={()=>{
				        	if(this.refs.passInput){
				        		this.refs.passInput.focus()
				        	}
				        }}
				      />
				      <Image source={require('../../images/email.png')} resizeMode='contain' style={{width:24,height:24}} />
		   			</View>
		   			<View style={styles.viewInput}>
		   				<TextInput
								ref="passInput"
								style={styles.textInput}
								underlineColorAndroid='transparent'
								autoCorrect={false}
								secureTextEntry={true}
								returnKeyType='done'
								clearButtonMode='while-editing'
								placeholder='Password'
								placeholderTextColor='#cccccc'
				        onChangeText={(text) => this.setState({pass:text})}
				        defaultValue={this.state.pass}
				        onSubmitEditing={()=>{
									this.doLogin()
				        }}
				      />
				      <Image source={require('../../images/lock.png')} resizeMode='contain' style={{width:24,height:24}} />
		   			</View>
						<TouchableHighlight
							underlayColor='transparent'
							style={styles.btnLogin}
							onPress={()=>{
								this.doLogin()
							}}>
							<Text style={styles.btnText}>LOGIN</Text>
						</TouchableHighlight>
					</View>
					<ActivityIndicator
						animating={this.state.loading}
						style={styles.indicator}
						color={Util.LOGO_COLOR}
						size="large"
					/>
				</View>
			</TouchableWithoutFeedback>
		)
	}

	doLogin(){
		//if running other login request (multi click login button)
		if(this.state.loading)
      return
		//Check valid email and password
    if(this.state.email.trim() === ''){
			Toast.ToastFailure("Please enter your email")
      return;
    }
    if(this.state.pass === ''){
			Toast.ToastFailure("Please enter password")
      return;
    }
		if(!Util.validateEmail(this.state.email.trim())){
			Toast.ToastFailure("Please enter valid email")
      return;
    }
		this.setState({loading:true})
		//Do request
    var body = {
      email:this.state.email.trim(),
      password:this.state.pass
    }
		Fetch.POST_UnSecure(APIConfig.login,body,(response)=>{
			this.setState({loading:false})
			if(!response.error){
				//Success
				this.props.navigation.navigate('Main');
			} else {
				//Failure
				Toast.ToastFailure("Login failure\n" + response.error)
				console.log('false');
			}
    },(error)=>{
      console.log(error);
      this.setState({loading:false})
      return
    })

	}
}

var styles = StyleSheet.create({
	contain:{
		flex:1,
		backgroundColor:'#ffffff',
		paddingTop:Util.OS === 'ios' ? 22 : 0
	},
	human_top:{
		width:96,
		height:123,
		position:'absolute',
		top:Util.OS === 'ios' ? 35 : 0,
		left:0
	},
	human_bottom:{
		width:72,
		height:71,
		position:'absolute',
		bottom:0,
		right:0
	},
	viewInput:{
		height:50,
		flexDirection:'row',
		alignItems:'center',
		backgroundColor:'#ffffff',
		marginTop:10,
		paddingLeft:10,
		paddingRight:10,
		borderWidth:1,
		borderColor:Util.LOGO_COLOR
	},
	textInput:{
		flex:1,
		color:'#000'
	},
	btnLogin:{
		height:50,
		marginTop:20,
		alignItems:'center',
		justifyContent:'center',
		backgroundColor:Util.LOGO_COLOR
	},
	btnText:{
		color:'#ffffff',
		fontSize:20,
		fontWeight:'bold'
	},
	indicator:{
		position:'absolute',
		top:200,
		left:0,
		right:0,
    height:80,
    alignItems:'center',
    justifyContent:'center',
		zIndex:1000
	}
})



//<Text style={{textAlign:'center',color:Util.LOGO_COLOR}}>Share your knowledge and help others</Text>
