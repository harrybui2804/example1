'use strict'
import React,{Component} from 'react'
import{
	StyleSheet,
	Dimensions,
	AsyncStorage,
	Alert,
	ScrollView,
	View,
	Image,
	Text,
	TextInput,
	TouchableWithoutFeedback
} from 'react-native'
import Env from '../../utils/env'
import { LOGO_COLOR, validateEmail, convertDateToTimestamp } from '../../utils/util'
import APIConfig from '../../utils/APIConfig'
import * as Fetch from '../../utils/fetch'
import Styles from '../../utils/styles'
import { ToastFailure } from '../../utils/ToastUtil'
import { Button } from 'react-native-elements'
import DatePicker from 'react-native-datepicker'

var dismissKeyboard = require('dismissKeyboard')
var {height, width} = Dimensions.get('window')

export default class SignUp extends Component{
	constructor(props){
		super(props)
		this.state = {
			FBInfo:null,
			email:'',
			birthday:'',
			occupation:'',
			company:'',
			expertises:'',
			expertise1:'',
			expertise2:'',
			expertise3:'',
			story:''
		}
	}

	componentDidMount(){
		const {info} = this.props.navigation.state.params
		if(info){
			this.setState({
				FBInfo:info,
				email:info.email,
				birthday:info.birthday ? info.birthday : ''
			})
		}
	}

	render(){
		var strHeader = 'Everyone of us is an expert. Please share your expertise in your profile'
		return(
			<TouchableWithoutFeedback
				onPress={dismissKeyboard}>
				<View style={{flex:1,backgroundColor:'#ffffff'}}>
				<ScrollView style={{flex:1,paddingLeft:20,paddingRight:20,paddingTop:20}}>
					<Image source={require('../../images/img-human-bottom.png')} resizeMode='contain' style={{width:72,height:91,position:'absolute',bottom:0,right:0,opacity:0.5,zIndex:1000}}/>
					<Text style={{color:'#000000',fontWeight:'bold'}}>{strHeader}</Text>
					<View style={{flex:1,marginTop:15}}>
						<TextInput style={Styles.input}
		          ref={(comp) => this.emailInput = comp}
		          autoCorrect={false}
		          defaultValue={this.state.email}
		          keyboardType='email-address'
		          onChangeText={(text) => {
		          	this.setState({email:text.trim().toLowerCase()})
		          }}
		          placeholder='Email'
		          placeholderTextColor='#cccccc'
		          returnKeyType='next'
		          underlineColorAndroid='transparent'
		          clearButtonMode='while-editing'
		          onSubmitEditing={()=> {
		          	this.occInput.focus()
		          }}
		        />
						<DatePicker
			        style={{height:40,width:width-40}}
			        date={this.state.birthday}
			        mode="date"
			        placeholder="Select your birthday"
			        format="YYYY-MM-DD"
			        confirmBtnText="Confirm"
			        cancelBtnText="Cancel"
			        showIcon={false}
			        customStyles={{
			          dateInput: {
			          	height:46,
			            borderWidth:0,
			            borderBottomWidth:1,
			            borderBottomColor:'#e6e6e6',
			            flex:1,
			            paddingLeft:5,
			            alignItems:'flex-start'
			          },
			          placeholderText:{
			          	fontSize:16,
			          	textAlign:'left'
			          },
			          dateText:{
			          	fontSize:16,
			          	textAlign:'left'
			          }
			        }}
			        onDateChange={(date) => {
			        	this.setState({birthday:date})
			        }}
			      />
						<View style={{flexDirection:'row'}}>
			        <TextInput style={[Styles.input,{flex:1}]}
			          ref={(comp) => this.occInput = comp}
			          autoCorrect={false}
			          keyboardType='default'
			          onChangeText={(text) => {
			          	this.setState({occupation:text.trim()})
			          }}
			          placeholder='Occupation'
			          placeholderTextColor='#cccccc'
			          returnKeyType='next'
			          underlineColorAndroid='transparent'
			          clearButtonMode='while-editing'
			          onSubmitEditing={()=> {
			          	this.compInput.focus()
			          }}
			        />
						</View>
						<TextInput style={Styles.input}
		          ref={(comp) => this.compInput = comp}
		          autoCorrect={false}
		          keyboardType='default'
		          onChangeText={(text) => {
		          	this.setState({company:text.trim()})
		          }}
		          placeholder='Company'
		          placeholderTextColor='#cccccc'
		          returnKeyType='next'
		          underlineColorAndroid='transparent'
		          clearButtonMode='while-editing'
		          onSubmitEditing={()=> {
		          	this.exp1Input.focus()
		          }}
		        />
		        <TextInput style={Styles.input}
		          ref={(comp) => this.exp1Input = comp}
		          autoCorrect={false}
		          keyboardType='default'
		          onChangeText={(text) => {
		          	var expertise = text.trim()
		          	if(this.state.expertise2.length > 0){
		          		expertise += expertise.length > 0 ? ',' + this.state.expertise2 : this.state.expertise2
		          	}
		          	if(this.state.expertise3.length > 0){
		          		expertise += expertise.length > 0 ? ',' + this.state.expertise3 : this.state.expertise3
		          	}
		          	var expertises = expertise.length > 0 ? expertise.split(',') : []
		          	var _expertises = expertises.map(function(x){ return x.charAt(0).toUpperCase() + x.slice(1) })
		          	this.setState({
		          		expertise1:text.trim(),
		          		expertises:_expertises
		          	})
		          }}
		          placeholder='Expertise'
		          placeholderTextColor='#cccccc'
		          returnKeyType='next'
		          underlineColorAndroid='transparent'
		          clearButtonMode='while-editing'
		          onSubmitEditing={()=> {
		          	this.exp2Input.focus()
		          }}
		        />
		        <TextInput style={Styles.input}
		          ref={(comp) => this.exp2Input = comp}
		          autoCorrect={false}
		          keyboardType='default'
		          onChangeText={(text) => {
		          	var expertise = this.state.expertise1
		          	if(text.trim().length > 0){
		          		expertise += expertise.length > 0 ? ',' + text.trim() : text.trim()
		          	}
		          	if(this.state.expertise3.length > 0){
		          		expertise += expertise.length > 0 ? ',' + this.state.expertise3 : this.state.expertise3
		          	}
		          	var expertises = expertise.length > 0 ? expertise.split(',') : []
		          	var _expertises = expertises.map(function(x){ return x.charAt(0).toUpperCase() + x.slice(1) })
		          	this.setState({
		          		expertise2:text.trim(),
		          		expertises:_expertises
		          	})
		          }}
		          placeholder='More expertise'
		          placeholderTextColor='#cccccc'
		          returnKeyType='next'
		          underlineColorAndroid='transparent'
		          clearButtonMode='while-editing'
		          onSubmitEditing={()=> {
		          	this.exp3Input.focus()
		          }}
		        />
		        <TextInput style={Styles.input}
		          ref={(comp) => this.exp3Input = comp}
		          autoCorrect={false}
		          keyboardType='default'
		          onChangeText={(text) => {
		          	var expertise = this.state.expertise1
		          	if(this.state.expertise2.length > 0){
		          		expertise += expertise.length > 0 ? ',' + this.state.expertise2 : this.state.expertise2
		          	}
		          	if(text.trim().length > 0){
		          		expertise += expertise.length > 0 ? ',' + text.trim() : text.trim()
		          	}
		          	var expertises = expertise.length > 0 ? expertise.split(',') : []
		          	var _expertises = expertises.map(function(x){ return x.charAt(0).toUpperCase() + x.slice(1) })
		          	this.setState({
		          		expertise3:text.trim(),
		          		expertises:_expertises
		          	})
		          }}
		          placeholder='More expertise'
		          placeholderTextColor='#cccccc'
		          returnKeyType='next'
		          underlineColorAndroid='transparent'
		          clearButtonMode='while-editing'
		          onSubmitEditing={()=> {
		          	this.storyInput.focus()
		          }}
		        />
		        <TextInput style={Styles.input}
		          ref={(comp) => this.storyInput = comp}
		          autoCorrect={false}
		          keyboardType='default'
		          onChangeText={(text) => {
		          	this.setState({story:text.trim()})
		          }}
		          placeholder='My story'
		          placeholderTextColor='#cccccc'
		          returnKeyType='done'
		          underlineColorAndroid='transparent'
		          clearButtonMode='while-editing'
		          onSubmitEditing={()=> {

		          }}
		        />
					</View>
					<View style={{marginTop:20,paddingBottom:30}}>
						<Button
						  small
						  buttonStyle={{height:36,borderRadius:8,backgroundColor:LOGO_COLOR,marginBottom:20}}
						  title='Done'
						  onPress={()=>{
						  	this.doSignUp()
						  }} />
						<Text style={{paddingLeft:30,paddingRight:30,fontSize:10}}>* By continuing you indicate that you have agreed of the Terms of Services and Privacy Policy</Text>
					</View>
				</ScrollView>
				</View>
			</TouchableWithoutFeedback>
		)
	}

	validate(){
		if(this.state.email.length === 0){
			ToastFailure('Please enter your email')
			return false
		} else {
			if(!validateEmail(this.state.email)){
				ToastFailure('Email is not valid')
				return false
			}
		}
		if(this.state.expertises.length === 0){
			ToastFailure('Please enter your expertises at least one')
			return false
		}
		return true
	}

	doSignUp(){
		if(this.validate()){
			var birthday = null
			if(this.state.birthday.length > 0){
				var d = Date.parse(this.state.birthday)
				birthday = convertDateToTimestamp(d)
			}
			var picture = this.state.FBInfo.picture !== null ? this.state.FBInfo.picture : ''
			var data = {
				facebook_id:this.state.FBInfo.fb_id,
				first_name:this.state.FBInfo.first_name,
				last_name:this.state.FBInfo.last_name,
				gender:this.state.FBInfo.gender,
				picture:picture,
        email: this.state.email,
        birthday:birthday,
        address:this.state.FBInfo.address,
        occupation:this.state.occupation,
        company:this.state.company,
        expertise:this.state.expertises,
        story:this.state.story
			}
			Fetch.POST_UnSecure(
				APIConfig.sign_up,
				data,
				(response)=>{
					console.log(response);
					if(response.error){
						if(response.error === 'email_used'){
							Alert.alert(
				        null,
				        'Email has already been used',
				        [
				          {text: 'OK'}
				        ]
				      )
						}
						Alert.alert(
				        null,
				        response.error,
				        [
				          {text: 'OK'}
				        ]
				      )
					} else {
						//save token and goto home
						AsyncStorage.setItem('access_token', response.access_token);
						Env.getInstance().setAppToken(response.access_token)
						this.props.navigation.navigate("Main")
					}
				},
				(error)=>{
					if(__DEV__){
						console.log('Error:', JSON.stringify(error))
					}
					this.setState({loading:false})
					Alert.alert(
		        null,
		        'Login failure',
		        [
		          {text: 'OK'}
		        ]
		      )
				},
				false
			)
		}
	}
}