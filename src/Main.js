'use strict'
import React,{Component} from 'react'
import {
	Platform,
	View
} from 'react-native'
import Env from './utils/env'
import { StackNavigator, SwitchNavigator } from 'react-navigation';
import Login from './views/user/login'
import * as Util from './utils/util'
import Home from './views/components/home'

const ViewScreen = ({ navigation }) => (
  <View navigation={navigation} />
);

export default class Main extends Component{
	constructor(props){
		super(props)
		Env.getInstance().setMain(this)
	}

	componentDidMount(){

	}

	render(){
		const MainNavigator = SwitchNavigator(
			{
		    Auth: Login,
		    Main: Home
	  	},
	  	{
	    	initialRouteName: 'Auth',
	  	}
		);
		return(
			<View style={{flex:1}}>
				<MainNavigator />
			</View>
		)
	}
}
