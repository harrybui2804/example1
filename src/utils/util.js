import {
	Platform,
	Dimensions
} from 'react-native'

export const LOGO_COLOR = '#ad172b'
export const OS = Platform.OS
export const DEVICE_SCREEN = Dimensions.get('window')

export const HEIGHT_TOPBAR = 65
export const LOGIN = "Login"
export const SIGNUP = "SignUp"

export const MAIN = 'Main'
export const DETAIL = 'Detail'
export const AVATAR = 'Avatar'

export function convertTimestampToDate(timestamp){
	return new Date(new Date(timestamp*1000).toLocaleString())
}

export function convertDateToTimestamp(time){
	return Number(new Date(time))/1000
}

export function timestampToString(timestamp){
	var date = new Date(new Date(timestamp*1000).toLocaleString())
	return date.getDate() + ' ' + monthNames[date.getMonth()] + ' ' + date.getHours() + ':' + date.getMinutes()
}

export function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export function validatePhoneNumber(phone) {
  var valid = false
	if(!isNaN(phone) && phone.length === 10)
		valid = true
	return valid
}
