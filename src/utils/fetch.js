'use strict';
import Env from './env'
import Internet from './internet'
import * as Toast from './ToastUtil'

var FETCH_TIMEOUT = 30000;

export function POST_UnSecure(url, body, onSuccess, onError){
  if(Internet.isOnline){
    new Promise(function(resolve, reject) {
      var timeout = setTimeout(function() {
          reject(new Error('Request timed out'));
      }, FETCH_TIMEOUT);

      fetch(url,{
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
      })
      .then(function(response) {
        clearTimeout(timeout);
        if (response){
          resolve(response.json());
        } 
        else{ 
          reject(new Error('Response error'));
        }
      })
    })
    .then(function(data) {
        onSuccess(data)
    })
    .catch(function(err) {
      onError({'success':false,'error':err.message})
    });
  } else {
    onError({'success':false,'error':'No internet access'})
    Toast.ToastFailure('No internet access')
  }
}

export function POST(url, body, onSuccess, onError){
  if(Internet.isOnline){
    var accessToken = Env.getInstance().getAppToken()
    new Promise(function(resolve, reject) {
      var timeout = setTimeout(function() {
          reject(new Error('Request timed out'));
      }, FETCH_TIMEOUT);

      fetch(url,{
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization':(`Bearer ${accessToken}`)
        }),
        body: JSON.stringify(body)
      })
      .then(function(response) {
        clearTimeout(timeout);
        if (response){
          resolve(response.json());
        } 
        else{ 
          reject(new Error('Response error'));
        }
      })
    })
    .then(function(data) {
        onSuccess(data)
    })
    .catch(function(err) {
      onError({'success':false,'error':err.message})
    });
  } else {
    onError({'success':false,'error':'No internet access'})
    Toast.ToastFailure('No internet access')
  }
}

export function POST_File(url, body, onSuccess, onError){
  if(Internet.isOnline){
    var accessToken = Env.getInstance().getAppToken()
    new Promise(function(resolve, reject) {
      var timeout = setTimeout(function() {
          reject(new Error('Request timed out'));
      }, FETCH_TIMEOUT);

      fetch(url,{
        method: 'POST',
        headers: new Headers({
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization':(`Bearer ${accessToken}`)
        }),
        body: body
      })
      .then(function(response) {
        clearTimeout(timeout);
        if (response){
          resolve(response.json());
        } 
        else{ 
          reject(new Error('Response error'));
        }
      })
    })
    .then(function(data) {
        onSuccess(data)
    })
    .catch(function(err) {
      onError({'success':false,'error':err.message})
    });
  } else {
    onError({'success':false,'error':'No internet access'})
    Toast.ToastFailure('No internet access')
  }
}

export function GET(url, onSuccess, onError){
  if(Internet.isOnline){
    var accessToken = Env.getInstance().getAppToken()
    new Promise(function(resolve, reject) {
      var timeout = setTimeout(function() {
        reject(new Error('Request timed out'));
      }, FETCH_TIMEOUT);

      fetch(url,{
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization':(`Bearer ${accessToken}`)
        })
      })
      .then(function(response) {
        clearTimeout(timeout);
        if (response){
          resolve(response.json())
        } 
        else{ 
          reject(new Error('Response error'));
        }
      })
    })
    .then(function(data) {
        // request succeed
        onSuccess(data)
    })
    .catch(function(err) {
      onError({'success':false,'error':err.message})
    });
  } else {
    onError({'success':false,'error':'No internet access'})
    Toast.ToastFailure('No internet access')
  }
}

export function PUT(url, body, onSuccess, onError){
  if(Internet.isOnline){
    var accessToken = Env.getInstance().getAppToken()
    new Promise(function(resolve, reject) {
      var timeout = setTimeout(function() {
          reject(new Error('Request timed out'));
      }, FETCH_TIMEOUT);

      fetch(url,{
        method: 'PUT',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization':(`Bearer ${accessToken}`)
        }),
        body: JSON.stringify(body)
      })
      .then(function(response) {
        clearTimeout(timeout);
        if (response){
          resolve(response.json());
        } 
        else{ 
          reject(new Error('Response error'));
        }
      })
    })
    .then(function(data) {
        onSuccess(data)
    })
    .catch(function(err) {
      onError({'success':false,'error':err.message})
    });
  } else {
    onError({'success':false,'error':'No internet access'})
    Toast.ToastFailure('No internet access')
  }
}

export function PUT_NoBody(url, onSuccess, onError){
  if(Internet.isOnline){
    var accessToken = Env.getInstance().getAppToken()
    new Promise(function(resolve, reject) {
      var timeout = setTimeout(function() {
          reject(new Error('Request timed out'));
      }, FETCH_TIMEOUT);

      fetch(url,{
        method: 'PUT',
        headers: new Headers({
          'Content-Type': 'application/json',
          'Authorization':(`Bearer ${accessToken}`)
        })
      })
      .then(function(response) {
        clearTimeout(timeout);
        if (response){
          resolve(response.json());
        } 
        else{ 
          reject(new Error('Response error'));
        }
      })
    })
    .then(function(data) {
        onSuccess(data)
    })
    .catch(function(err) {
      onError({'success':false,'error':err.message})
    });
  } else {
    onError({'success':false,'error':'No internet access'})
    Toast.ToastFailure('No internet access')
  }
}

/*
export async function GET(url, onSuccess, onError){
  var accessToken = Env.getInstance().getAppToken()
	try {
		let data = await fetch(url, {
		  method: 'GET',
		  headers: new Headers({
		    'Content-Type': 'application/json',
		    'Authorization':(`Bearer ${accessToken}`)
		  })
		});
		return onSuccess(await data.json())
	} catch(error){
		onError(error)
	}
}

export async function POST_UnSecure(url, body, onSuccess, onError) {
  var headers = new Headers({
    'Content-Type': 'application/json'
  })
  try {
    let data = await fetch(url, {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(body)
    });
    return onSuccess(await data.json())
  } catch(error) {
    onError(error)
  }
}

export async function POST(url, body, onSuccess, onError) {
  var accessToken = Env.getInstance().getAppToken()
	var headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization':(`Bearer ${accessToken}`)
  })
	try {
		let data = await fetch(url, {
		  method: 'POST',
		  headers: headers,
		  body: JSON.stringify(body)
		});
		return onSuccess(await data.json())
	} catch(error) {
		onError(error)
	}
}


export async function POSTFile(url, body, onSuccess, onError) {
	var accessToken = Env.getInstance().getAppToken()
  try {

		let data = await fetch(url, {
		  method: 'POST',
		  headers: new Headers({
		  	'Accept': 'application/json',
		    'Content-Type': 'multipart/form-data',
		    'Authorization':(`Bearer ${accessToken}`)
		  }),
		  body: body
		});
		return onSuccess(await data.json())
  } catch (error){
      onError(error);
  }
}
export async function PUT(url, body, onSuccess, onError) {
	var accessToken = Env.getInstance().getAppToken()
  try {
    let data = await fetch(url, {
		  method: 'PUT',
		  headers: new Headers({
		    'Content-Type': 'application/json',
		    'Authorization':(`Bearer ${accessToken}`)
		  }),
          body: JSON.stringify(body)
		});
    return onSuccess(await data.json())
  } catch (error){
    onError(error);
  }
}

/*
export async function put(url, successCallBack, failCallBack) {
    try {
        const Header1 = {
            //'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': (`Bearer ${global.userToken}`)
        };
        let data = await RNFetchBlob.fetch('PUT', url, Header1);
        if (data.respInfo.status === 200) {
            return successCallBack(await data.json());
        } else {
            return failCallBack(data.json());
        }
    } catch (error) {
        failCallBack(error);
    }
}

export function header(Obj) {
    console.log(Obj);
    return fetch('','',Obj,'POST');
}
*/
/***
 * @param url       请求网址
 * @param params    请求参数
 * @param method    请求类型
 * @param header    请求头
 * @returns {Promise}
 */
 /*
export function fetch(url, params, header, method='GET') {
    // console.log(url);
    // console.log(params);
    let rt_method = method;

    if (header){

        return;
    }

    let URL = verifyURL(url);
    let postParams = {
        rt_method,
        'User-Agent': 'ShunLian iPhone 9.0.1/1.0.0 ',
        'X-Device-ID': 'FC1D511A-70FA-4ABC-8E7A-F1AACCBF9BAA',
        'Accept-Encoding': 'gzip, deflate',
        'X-Ip': '192.168.1.1',
    };
    console.log(postParams);
    if (params) {
        if (rt_method !== 'GET') {
            postParams.body = JSON.stringify(params);
        } else {
            let keys = Object.keys(params);
            if (keys.length) {
                let args = keys.map(item => {
                    return `${item}=${params[item]}`
                });
                URL = `${URL}?${args.join('&')}`;
            }
        }
    }

    return new Promise((resolve, reject) => {
        RNFetch.fetch(rt_method, URL, postParams)
            .then((response) => {
                // console.log(response);
                if (response.respInfo.status === 200) {
                    return response.json()
                } else {
                    reject(response);
                }
            })
            .then((response) => {
                console.log(response);
                resolve(response)
            })
            .catch((error) => {
                console.log(error);
                reject(error);
            })
    });
}

// 检查url
function verifyURL(url) {
    // 验证url
    let verifyURL;

    // 判断是否填写了url地址
    if (!url) {
        console.log('url为空');
        return alert('url为空,请填写url');
    } else {
        // 处理网址中的前后两端的空格
        verifyURL = url.replace(/(^s*)|(s*$)/g, '');
        return verifyURL;
    }

}
*/
