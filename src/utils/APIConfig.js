'use strict'

const HOST = 'https://reqres.in'

const APIConfig = {
	login 		: HOST + '/api/login',
	list_user	: HOST + '/api/users?page={params}',
	user_info : HOST + '/api/users/{param}'
};

export default APIConfig
