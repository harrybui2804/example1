'use strict'

import Toast from 'react-native-root-toast';
import {LOGO_COLOR} from './util'

let toast;

export const ToastShort = (content,onShow,onShown,onHide,onHidden) =>{
    if(toast !== undefined){
        Toast.hide(toast);
    }
    toast = Toast.show(content.toString(),{
        duration: Toast.durations.SHORT,
        position: Toast.positions.TOP,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        onShow: onShow,
        onShown:onShown,
        onHide: onHide,
        onHidden:onHidden
    })
};
export const ToastFailure = (content,onShow,onShown,onHide,onHidden) =>{
    if(toast !== undefined){
        Toast.hide(toast);
    }
    toast = Toast.show(content.toString(),{
        duration: Toast.durations.SHORT,
        position: Toast.positions.CENTER,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        onShow: onShow,
        onShown:onShown,
        onHide: onHide,
        onHidden:onHidden,
        backgroundColor: LOGO_COLOR,
        textColor: 'white',
        shadowColor: 'white',
        zIndex:500000
    })
};
export const ToastSuccess = (content,onShow,onShown,onHide,onHidden) =>{
    if(toast !== undefined){
        Toast.hide(toast);
    }
    toast = Toast.show(content.toString(),{
        duration: Toast.durations.SHORT,
        position: Toast.positions.CENTER,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        onShow: onShow,
        onShown:onShown,
        onHide: onHide,
        onHidden:onHidden,
        backgroundColor: '#77b936',
        textColor: 'white',
        shadowColor: 'white',
    })
};


export const ToastLong = (content,onShow,onShown,onHide,onHidden) => {
    /*if(toast !==undefined ){
        Toast.hide(toast);
    }
    toast = Toast.show(content.toString(),{
        duration: Toast.durations.LONG,
        position: Toast.positions.CENTER,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        onShow: onShow,
        onShown:onShown,
        onHide: onHide,
        onHidden:onHidden
    })*/
};