'use strict'

var env = null

export default class Env {
	constructor(){
    this._user = null
    this._appToken = null
    this._switchNavigator = null
    this._homeNavigator = null
    this._topbar = null
    this._tabbar = null
    this._main = null
  }

  setUser(user){
    this._user = user
  }
  getUser(){
    return this._user
  }

  setAppToken(token){
    this._appToken = token
  }
  getAppToken(){
    return this._appToken
  }

  setSwitchNavigator(navigator){
    this._switchNavigator = navigator
  }
  getSwitchNavigator(){
    return this._switchNavigator
  }

  setHomeNavigator(navigator){
    this._homeNavigator = navigator
  }
  getHomeNavigator(){
    return this._homeNavigator
  }

  setTopbar(topbar){
    this._topbar = topbar
  }
  getTopbar(){
    return this._topbar
  }

  setTabbar(tab){
    this._tabbar = tab
  }
  getTabbar(){
    return this._tabbar
  }

  setMain(view){
    this._main = view
  }
  getMain(){
    return this._main
  }

  static getInstance(){
    if(env == null){
        env = new Env()
    }
    return env;
	}
}
